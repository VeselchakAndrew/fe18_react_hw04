import React, {Component} from 'react';
import "./CustomButton.scss"

class CustomButton extends Component {
    render() {
        const {customBtnClass, btnAction} = this.props;
        return (
            <div className={customBtnClass} onClick={btnAction}>

            </div>
        );
    }
}

export default CustomButton;