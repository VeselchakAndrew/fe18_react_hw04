import React, {useEffect} from 'react';
import ItemsList from "../../ItemsList/ItemsList";
import {useDispatch, useSelector} from "react-redux";
import {matchData, updateData, getGoodID} from "../../../redux/actions/itemsAction";

const Cart = () => {
    const dispatch = useDispatch();
    const goodsInCart = useSelector(state => state.items.goodsInCart);
    const isUpdated = useSelector(state => state.items.isUpdated);

    //Получаем начальные данные
    useEffect(() => {
        dispatch(getGoodID("Cart"));
        dispatch(matchData("Cart", "CART"));
        dispatch(updateData(false))
    }, [isUpdated]);


    return (
        <>
            {goodsInCart.length === 0 &&
            <div className="empty_page">
                <h2>Корзина пустая</h2>
            </div>}
            <ItemsList goodsData={goodsInCart}
                       isDeleteExist={true}
            />
        </>
    );
};

export default Cart;