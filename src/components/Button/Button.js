import React, {Component} from 'react';
import PropTypes from "prop-types";
import "./Button.scss"

class Button extends Component {
    render() {
        const {children, btnClass, btnAction} = this.props;
        return (
            <button className={btnClass} onClick={btnAction}>{children}</button>
        );
    }
}

Button.propTypes = {
    children: PropTypes.string,
    btnClass: PropTypes.string,
    btnAction: PropTypes.func.isRequired
}

Button.defaultProps = {
    children: "BUTTON",
    btnClass: "add_btn"
}

export default Button;