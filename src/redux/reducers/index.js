import {combineReducers} from "redux";
import "./itemsReducer"
import itemsReducer from "./itemsReducer";
import modalReducer from "./modalReducer";

const rootReducer =  combineReducers({
    items: itemsReducer,
    modals: modalReducer
});

export default rootReducer;

