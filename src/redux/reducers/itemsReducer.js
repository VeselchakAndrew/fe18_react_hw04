import {
    GET_GOODS_FROM_JSON,
    GET_GOODS_ID_IN_CART,
    GET_GOODS_ID_IN_FAVOURITE,
    GET_GOODS_IN_CART,
    GET_GOODS_IN_FAVOURITE,
    ADD_GOODS_IN_CART,
    REMOVE_GOODS_FROM_CART,
    SET_GOODS_IN_FAVOURITE,
    IS_UPDATED,
    IS_LOADING
} from "../itemsActionTypes";

const initialState = {
    goods: [],
    goodsInCartID: [],
    goodsInFavouriteID: [],
    goodsInCart: [],
    goodsInFavourite: [],
    isUpdated: true,
    isLoading: false
}

const itemsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_GOODS_FROM_JSON:
            return {
                ...state, goods: action.payload, isLoading: false
            }

        case GET_GOODS_ID_IN_CART:
            return {
                ...state, goodsInCartID: action.payload
            }

        case GET_GOODS_ID_IN_FAVOURITE:
            return {
                ...state, goodsInFavouriteID: action.payload
            }

        case GET_GOODS_IN_CART:
            return {
                ...state, goodsInCart: action.payload
            }

        case GET_GOODS_IN_FAVOURITE:
            return {
                ...state, goodsInFavourite: action.payload
            }

        case ADD_GOODS_IN_CART:
            return {
                ...state, goodsInCartID: action.payload
            }

        case REMOVE_GOODS_FROM_CART:
            return {
                ...state, goodsInCartID: action.payload
            }

        case SET_GOODS_IN_FAVOURITE:
            return {
                ...state, goodsInFavouriteID: action.payload
            }

        case IS_UPDATED:
            return {
                ...state, isUpdated: action.payload
            }

        case IS_LOADING:
            return {
                ...state, isLoading: action.payload
            }

        default:
            return state
    }
}

export default itemsReducer;